package com.craftinginterpreters.jlox;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScannerTest {

    @Test
    void givenEmptyString_whenScanning_thenTokensContainsEOF() {
        Scanner scanner = new Scanner("");
        List<Token> tokens = scanner.scanTokens();
        assertNotEquals(null, tokens);
        assertEquals(1, tokens.size());
        Token token = tokens.get(0);
        assertEquals(TokenType.EOF, token.type);
        assertEquals("", token.lexeme);
        assertEquals(null, token.literal);
        assertEquals(1, token.line);
    }

    @Test
    void givenSingleCharToken_whenScanning_thenTokensContainGivenSingleCharToken() {
        Scanner scanner = new Scanner("*;+-.,}{)(");
        List<Token> tokens = scanner.scanTokens();
        assertEquals(TokenType.STAR, tokens.get(0).type);
        assertEquals(TokenType.SEMICOLON, tokens.get(1).type);
        assertEquals(TokenType.PLUS, tokens.get(2).type);
        assertEquals(TokenType.MINUS, tokens.get(3).type);
        assertEquals(TokenType.DOT, tokens.get(4).type);
        assertEquals(TokenType.COMMA, tokens.get(5).type);
        assertEquals(TokenType.CLOSE_BRACE, tokens.get(6).type);
        assertEquals(TokenType.OPEN_BRACE, tokens.get(7).type);
        assertEquals(TokenType.CLOSE_PAREN, tokens.get(8).type);
        assertEquals(TokenType.OPEN_PAREN, tokens.get(9).type);
        assertEquals(TokenType.EOF, tokens.get(10).type);
    }

    @Test
    void givenMultiCharPotential_whenScanning_thenReturnMultiCharToken() {
        Scanner scanner = new Scanner("!!===!====");
        List<Token> tokens = scanner.scanTokens();
        assertEquals(7, tokens.size());

        // Test TokenType
        assertEquals(TokenType.BANG, tokens.get(0).type);
        assertEquals(TokenType.BANG_EQUAL, tokens.get(1).type);
        assertEquals(TokenType.EQUAL_EQUAL, tokens.get(2).type);
        assertEquals(TokenType.BANG_EQUAL, tokens.get(3).type);
        assertEquals(TokenType.EQUAL_EQUAL, tokens.get(4).type);
        assertEquals(TokenType.EQUAL, tokens.get(5).type);
        assertEquals(TokenType.EOF, tokens.get(6).type);

        // Test Token Lexeme
        assertEquals("!", tokens.get(0).lexeme);
        assertEquals("!=", tokens.get(1).lexeme);
        assertEquals("==", tokens.get(2).lexeme);
        assertEquals("!=", tokens.get(3).lexeme);
        assertEquals("==", tokens.get(4).lexeme);
        assertEquals("=", tokens.get(5).lexeme);
        assertEquals("", tokens.get(6).lexeme);
    }

    @Test
    void givenUnterminatedString_whenScanning_thenScannerShouldError() {
        Scanner scanner = new Scanner("\"thing");
        // Not sure this is a good test or good code
        // redirect system output
        // From https://stackoverflow.com/questions/26059003/junit-testing-system-err-print
        // This is instead testing if Lox is submitting errors correctly, but indirectly tesing
        // the error functionality of Scanner...eh, no lo se, Rick. Parece falso.
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(outContent));
        scanner.scanTokens(); // trigger error of "Unterminated string"

        assertEquals("[line 1] Error: Unterminated string.\n", outContent.toString());
    }

    @Test
    void givenMultiLineInput_whenScanning_thenScannerIdentifiesCorrectLine() {
        String source = "thing + thing2 = 9;\n" +
                "thing4; thing5-thing6==\"false\";\n" +
                "thing7/(thing8*thing9)={};\n";

        Scanner scanner = new Scanner(source);
        List<Token> tokens = scanner.scanTokens();
        assertEquals(26, tokens.size());

        Token token = tokens.get(0);
        assertEquals(TokenType.IDENTIFIER, token.type);
        assertEquals("thing", token.lexeme);
        assertEquals(1, token.line);

        token = tokens.get(1);
        assertEquals(TokenType.PLUS, token.type);
        assertEquals("+", token.lexeme);
        assertEquals(1, token.line);
        // only string should have a literal
        assertEquals(null, token.literal);

        token = tokens.get(4);
        assertEquals(TokenType.NUMBER, token.type);
        assertEquals("9", token.lexeme);

        token = tokens.get(6);
        assertEquals(TokenType.IDENTIFIER, token.type);
        assertEquals("thing4", token.lexeme);
        assertEquals(2, token.line);

        token = tokens.get(12);
        assertEquals("\"false\"", token.lexeme);
        assertEquals("false", token.literal);
    }
}
